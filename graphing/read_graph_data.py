import csv
import json
# import itertools
import collections
# from bokeh.plotting import ColumnDataSource


class RegressionDataSource(object):

    def __init__(self, regr_data_path):
        """
        """
        # read the regression data
        self.regr_data_fieldnames = ["Time stamp", "Zone ID", "Macropixel ID", "Range mm", "Datalog type",
                                     "Peak rate kcps", "Effective Spads", "Coverage"]
        with open(regr_data_path) as regr_data_csvfile:
            # read each row and store as dict of first row names and value
            self.regr_data_dict = csv.DictReader(regr_data_csvfile, self.regr_data_fieldnames)

            self.all_regr_data = {}
            previous_timestamp = -1
            regr_data_frame = None
            self.unique_timestamps = []
            for row in self.regr_data_dict:
                # convert the row to int, float
                for key in row:
                    row[key] = float(row[key]) if key == "Coverage" else int(row[key])

                current_timestamp = int(row["Time stamp"])
                if current_timestamp != previous_timestamp:
                    # add the previous data frame to the all regression data dict
                    if regr_data_frame is not None:
                        self.all_regr_data[previous_timestamp] = regr_data_frame
                    # reset and start collecting the data frame
                    regr_data_frame = [row]
                    self.unique_timestamps.append(current_timestamp)
                    previous_timestamp = current_timestamp
                else:
                    regr_data_frame.append(row)

        # read the settings data for the macropixel positions
        #print "meh"

    def unique_list(self, seq):
        """
        Create a unique list, preserving order
        See: https://www.peterbe.com/plog/uniqifiers-benchmark
        """
        seen = {}
        result = []
        for item in seq:
            if item in seen:
                continue
            seen[item] = 1
            result.append(item)
        return result

    def get_frame(self, timestamp, previous=False, next_frame=False):
        """
        Get the current, next, or previous frame's worth of data
        (from the current/next/previous 'timestamp' value)
        """
        if previous:
            reqd_timestamp = self.unique_timestamps[self.unique_timestamps.index(timestamp) - 1]
        elif next_frame:
            reqd_timestamp = self.unique_timestamps[self.unique_timestamps.index(timestamp) + 1]
        else:
            reqd_timestamp = timestamp

        regr_data_frame = collections.defaultdict(list)
        for row in self.all_regr_data[reqd_timestamp]:
            for key, value in row.items():
                if key in ["Macropixel ID", "Range mm", "Coverage"]:
                    regr_data_frame[key].append(value)

        # regr_data_column_data_source = ColumnDataSource(data=regr_data_frame)

        return reqd_timestamp, regr_data_frame

    def macropixel_norm_silicon_coords(self, signal_map_lut_path):
        """
        Get the normalised coordinates of the macropixels on silicon
        """
        with open(signal_map_lut_path) as lut_json_file:
            lut_json = json.load(lut_json_file)
        coords_list = lut_json["macropixelSiliconCenterCordinates"]
        coords_dict = {}
        coords_dict["x"] = [float(coord[u"x"]) for coord in coords_list]
        coords_dict["y"] = [float(coord[u"y"]) for coord in coords_list]
        return coords_dict

    def reorder_macropixel_coords(self, coords_dict, macropixel_ids):
        """
        Reorder the macropixel coords to the same order as the list
        of macropixel IDs
        """
        reordered_coords_dict = {}
        reordered_coords_dict["x"] = [coords_dict["x"][macropixel_id] for macropixel_id in macropixel_ids]
        reordered_coords_dict["y"] = [coords_dict["y"][macropixel_id] for macropixel_id in macropixel_ids]
        return reordered_coords_dict


if __name__ == "__main__":
    regr_data_path = "sample_data.csv"
    regr_data_src = RegressionDataSource(regr_data_path)
    ts, cds = regr_data_src.get_frame(4800, next_frame=True)
    print ts, cds

    signal_map_lut_path = "8x8 (2x4).json"
    macropixel_coords = regr_data_src.macropixel_norm_silicon_coords(signal_map_lut_path)
    print macropixel_coords
    reorder_macropix_coords = regr_data_src.reorder_macropixel_coords(macropixel_coords, cds["Macropixel ID"])
    print reorder_macropix_coords
