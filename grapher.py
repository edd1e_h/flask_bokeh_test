from bokeh.embed import components, autoload_server
from flask import Flask, render_template, render_template_string, request, redirect, flash, url_for
from bokeh.client import pull_session, push_session
from bokeh.plotting import figure
import csv
import subprocess
import atexit
import threading
import time
import graphing.main

app = Flask(__name__)
app.secret_key = r'A\xa4T\x93\xe0\x11\x15\x8dL\x8d\xba\x9d\xdbFiXNU\xf4\xda\x90\xf1%\x83'
ALLOWED_EXTENSIONS = ["csv"]

auto_graph_html = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <div class="bk-root">
      {{ bokeh_script|safe }}
    </div>
  </body>
</html>
"""

# bokeh_process = subprocess.Popen(
#     ['bokeh', 'serve',
#      '--log-level=debug',
#      # '--port=5111',
#      '--host=flask-bokeh-tryout.herokuapp.com:5006',
#      '--host=localhost:5006',
#      '--host=*',
#      '--address=0.0.0.0',
#      '--use-xheaders',
#      '--allow-websocket-origin=localhost:33507',
#      '--allow-websocket-origin=flask-bokeh-tryout.herokuapp.com',
#      'graphing',
#      # '--args', '2x4-Simple_[Thin]-W-E-W_short_test', 'readout_upsampling'
#      # '--args', '', '', 'True'
#      ], stdout=subprocess.PIPE)
# print "Starting bokeh server..."

session_thread = None


# @atexit.register
# def kill_server():
#     bokeh_process.kill()


@app.route('/')
def home():
    return redirect('/index')


@app.route('/hello')
def hello_world():
    return 'Hello, World!'


@app.route('/index')
def index():
    flash('Flash test')
    return render_template('index.html')


@app.route('/graph', methods=['GET', 'POST'])
def graph():
    plot = figure(title='Test')
    script, div = components(plot)

    if request.method == 'POST':
        # check if the post request has the file part
        if 'csv_file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        csv_file = request.files['csv_file']
        # if user does not select file, browser also
        # submit a empty part without filename
        if csv_file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if csv_file:
            valid, msg = validate_csv_file(csv_file)
            if not valid:
                flash('CSV file invalid - %s' % msg)
                return redirect(request.url)
            data_dict = csv.DictReader(csv_file)
            x, y = [], []
            for row in data_dict:
                x.append(row["x"])
                y.append(row["y"])
            plot.line(x, y)
            script, div = components(plot)

    return render_template('graph.html', script=script, div=div)


@app.route('/auto_graph', methods=['GET', 'POST'])
def auto_graph():
    global session_thread
    if session_thread:
        session_thread.stop()

    bokeh_script = None
    if request.method == 'POST':
        # check if the post request has the file part
        if 'csv_file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        csv_file = request.files['csv_file']
        # if user does not select file, browser also
        # submit a empty part without filename
        if csv_file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if csv_file:
            valid, msg = validate_csv_file(csv_file)
            if not valid:
                flash('CSV file invalid - %s' % msg)
                return redirect(request.url)
#            session = pull_session(url='http://bokeh-grapher.herokuapp.com',
#                                   app_path='/graphing',
#                                   )
            # upsampler_graph = graphing.main.UpsamplerGraph()
            # session = push_session(upsampler_graph.curr_doc, app_path='/graphing')  # , session_id=session.id)
            # session_thread = SessionThread(csv_file)
#            session_thread = SessionThread(None)
#            session_id = session_thread.session.id
#            session_thread.start()
#            time.sleep(1)
            # bokeh_script = autoload_server(model=None, url='http://localhost:5006', app_path="/graphing",
            #                                session_id=session_id)
    bokeh_script = autoload_server(model=None,
                                   url='http://bokeh-grapher.herokuapp.com/',
                                   app_path="/graphing",
#                                   session_id=session_id,
                                   )
            # session.loop_until_closed()  # Blocking

    return render_template('auto_graph.html', bokeh_script=bokeh_script)
    # return render_template_string(auto_graph_html, bokeh_script=bokeh_script)


def session_worker():
    """
    Handles the thread for the session, which would otherwise be blocking
    """
    print "Start"
    # time.sleep(10)
    upsampler_graph = graphing.main.UpsamplerGraph()
    session = push_session(upsampler_graph.curr_doc, app_path='/graphing')  # , session_id=session.id)
    session.loop_until_closed()  # Blocking
    print "Stop"


class SessionThread(threading.Thread):
    """Thread class with a stop() method. The thread itself has to check
    regularly for the stopped() condition."""

    def __init__(self, filepath):
        super(SessionThread, self).__init__()
        self._stop = threading.Event()
        upsampler_graph = graphing.main.UpsamplerGraph(filepath)
        # self.session = push_session(upsampler_graph.curr_doc, url='http://localhost:5006', app_path='/graphing')  # , session_id=session.id)
        self.session = push_session(upsampler_graph.curr_doc,
                                    url='http://bokeh-grapher.herokuapp.com/',
                                    app_path='/graphing')  # , session_id=session.id)

    def run(self):
        self.session.loop_until_closed()  # Blocking

    def stop(self):
        print "Stopping"
        self._stop.set()

    def stopped(self):
        return self._stop.isSet()


@app.route('/upload', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'csv_file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        csv_file = request.files['csv_file']
        # if user does not select file, browser also
        # submit a empty part without filename
        if csv_file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if csv_file:  # and allowed_file(file.filename):
            # filename = secure_filename(file.filename)
            # file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            return redirect(url_for('graph',
                                    filename="bla"))
    return render_template('upload.html')


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


def validate_csv_file(csvfile):
    try:
        dialect = csv.Sniffer().sniff(csvfile.read(1024))
        # Perform various checks on the dialect (e.g., lineseparator,
        # delimiter) to make sure it's sane
        if dialect.delimiter != ",":
            return False, "Invalid delimiter"
        if dialect.lineterminator != "\r\n":
            return False, "Invalid line terminator"

        # Don't forget to reset the read position back to the start of
        # the file before reading any entries.
        csvfile.seek(0)
        return True, "Csv file OK"
    except csv.Error:
        return False, "Invalid file"


if __name__ == "__main__":
    app.run(port=33507,
            # debug=True,
            )
