# import pandas as pd

from bokeh.io import curdoc, output_file, save, show, output_server
from bokeh.layouts import layout
from bokeh.models import (ColumnDataSource, HoverTool, Text, Circle,
                          SingleIntervalTicker, Slider, Button)
import bokeh.palettes
from bokeh.plotting import figure
from bokeh.client import push_session
import numpy as np
import os
import sys
# print sys.path
import read_graph_data
from surface3d import Surface3d


class UpsamplerGraph(object):
    """
    Generate a graph using the upsampler data to run on bokeh server
    Requires bokeh >= 0.12.0

    At the command line type the following to run:
    bokeh serve graphing --show --args <name of sim directory> <name of subtype of algo>
    """
    def __init__(self, regr_data_path=None, signal_map_lut_path=None, graph_in_3d=False):
        self.graph_in_3d = graph_in_3d
        self.curr_doc = curdoc()
        self.colour_palette = bokeh.palettes.viridis(191)
        base_data_path = os.path.dirname(os.path.abspath(__file__))
        if not regr_data_path:
            regr_data_path = os.path.join(base_data_path, "regression.csv")
        print "Getting data from: ", regr_data_path
        self.regr_data_src = read_graph_data.RegressionDataSource(regr_data_path)
        self.first_frame_timestamp = self.regr_data_src.unique_timestamps[0]
        self.last_frame_timestamp = self.regr_data_src.unique_timestamps[-2]  # The very last timestamp is not useable
        # scale the size vs coverage
        self.size_scaler = 1.0 if self.graph_in_3d else (2.0 / 8.0) / 2.0

        # append the macropixel positions
        if not signal_map_lut_path:
            signal_map_lut_path = os.path.join(base_data_path, "8x8 (2x4).json")
        print "Getting signal map LUT from: ", signal_map_lut_path
        self.macropixel_coords = self.regr_data_src.macropixel_norm_silicon_coords(signal_map_lut_path)

        self.options = {
            'width':          '600px',
            'height':         '600px',
            'style':          'dot-size',
            'showPerspective': True,
            'showGrid':        True,
            'keepAspectRatio': True,
            'verticalRatio':   1.0,
            'legendLabel':     'Coverage',
            'cameraPosition':  {
                'horizontal': -0.35,
                'vertical':    0.22,
                'distance':    1.8,
            },
            'zMin':            0,
            'zMax':            200,
            'dotSizeRatio':    0.03,
        }

        if self.graph_in_3d:
            ds = ColumnDataSource(data=self.current_frame(self.first_frame_timestamp))
            ds.data["Coverage"] = [1.0] * len(ds.data["Coverage"])  # Dummy data change to force a refresh of the display (HACK)
            plot = self.plot_glyphs = Surface3d(
                x="x", y="y", z="Clipped range mm", color="Coverage",
                data_source=ds, options=self.options
            )
        else:
            # plot = figure(x_range=(1, 9), y_range=(20, 100), title='Gapminder Data', plot_height=300)
            plot = figure(title='Upsampled zones (silicon view)')
            # plot.xaxis.ticker = SingleIntervalTicker(interval=1)
            plot.xaxis.axis_label = "x"
            # plot.yaxis.ticker = SingleIntervalTicker(interval=20)
            plot.yaxis.axis_label = "y"
            # label = Label(x=1.1, y=18, text=str(years[0]), text_font_size='70pt', text_color='#eeeeee')
            # plot.add_layout(label)
            self.plot_glyphs = plot.circle(
                x="x", y="y", radius="Coverage",
                source=ColumnDataSource(data=self.current_frame(self.first_frame_timestamp)),
                fill_color="Colours"
            )  # , fill_alpha=0.8), line_color='#7c7e71', line_width=0.5, line_alpha=0.5)
            # plot.add_tools(HoverTool(tooltips="@index", renderers=[self.cr]))

            # this draws a custom legend
            # tx, ty = 6.3, 95
            # for i, region in enumerate(regions):
            #     plot.add_glyph(Text(x=tx, y=ty, text=[region], text_font_size='10pt', text_color='#666666'))
            #     plot.add_glyph(Circle(x=tx-0.1, y=ty+2, fill_color=Spectral6[i], size=10, line_color=None, fill_alpha=0.8))
            #     ty -= 5

        self.slider = Slider(start=self.first_frame_timestamp, end=self.last_frame_timestamp,
                             value=self.first_frame_timestamp, step=50,
                             title="Timestamp")  # TODO: remove hardcoded step value
        self.slider.on_change('value', self.slider_update)

        self.button = Button(label='Play')
        self.button.on_click(self.animate)

        # layout = vplot(plot, slider, button)
        self.graph_layout = layout([
            [plot],
            [self.slider, self.button],
        ])  # , sizing_mode='scale_width')

        self.curr_doc.add_root(self.graph_layout)
        #self.curr_doc.add_periodic_callback(self.animate_update, 200)
        self.curr_doc.title = "Depth upsampler"
        # show(self.graph_layout)

        # open a session to keep our local document in sync with server
        # self.session = push_session(curdoc())#, app_path='/graphing')
        # self.session.loop_until_closed()

    @staticmethod
    def compute(t):
        x = np.arange(0, 300, 20)
        y = np.arange(0, 300, 20)
        xx, yy = np.meshgrid(x, y)
        xx = xx.ravel()
        yy = yy.ravel()
        value = np.sin(xx / 50 + t / 10) * np.cos(yy / 50 + t / 10) * 50 + 50
        return dict(x=xx, y=yy, z=value, color=value)

    def animate_update(self, *args, **kwargs):
        timestamp = self.slider.value + 50  # TODO: remove hardcoded value
        if timestamp > self.last_frame_timestamp:
            timestamp = self.first_frame_timestamp
        self.slider.value = timestamp

    def slider_update(self, attrname, old, new):
        timestamp = self.slider.value
        # label.text = str(year)
        # BEST PRACTICE --- update .data in one step with a new dict
        self.plot_glyphs.data_source.data = self.current_frame(timestamp)

    def current_frame(self, timestamp):
        """
        Get data for the frame at timestamp
        """
        # reorder the coords as per the regression data order
        curr_frame = self.regr_data_src.get_frame(timestamp)[1]
        reordered_macropixel_coords = self.regr_data_src.reorder_macropixel_coords(self.macropixel_coords,
                                                                                   curr_frame["Macropixel ID"])
        curr_frame.update(reordered_macropixel_coords)
        curr_frame["Coverage"] = [sze * self.size_scaler for sze in curr_frame["Coverage"][:]]
        # curr_frame["Colours"] = [min(100, max(0, rgc / 2.0)) for rgc in curr_frame["Range mm"][:]]
        curr_frame["Colours"] = [self.range_colour(rgc) for rgc in curr_frame["Range mm"][:]]
        curr_frame["Clipped range mm"] = [min(200, rgc) for rgc in curr_frame["Range mm"][:]]
        return curr_frame

    def animate(self):
        if self.button.label == 'Play':
            self.button.label = 'Pause'
            self.curr_doc.add_periodic_callback(self.animate_update, 200)
        else:
            self.button.label = 'Play'
            self.curr_doc.remove_periodic_callback(self.animate_update)

    @staticmethod
    def range_colour(range_mm, scale=1, number_of_colours=191):
        """
        Return a certain colour for a range value
        """
        colour_palette = bokeh.palettes.viridis(number_of_colours)
        return colour_palette[min(
            max(0, int(range_mm * scale)),
            (number_of_colours - 1))
        ]


# Call the class (note that "bokeh serve graphing --show" does not set __name__ == "__main__")
# if len(sys.argv) == 4:
#     if sys.argv[3] == "True":
#         UpsamplerGraph(sys.argv[1], sys.argv[2], True)
#     else:
#         UpsamplerGraph(sys.argv[1], sys.argv[2], False)
# elif len(sys.argv) == 3:
#     UpsamplerGraph(sys.argv[1], sys.argv[2], False)
# elif len(sys.argv) == 2:
#     UpsamplerGraph(sys.argv[1], None, False)
# else:
#     print "Warning: Not enough arguments, using default data."
#     print "Pass the path to the regression data (and optional flag to use 3D)"
#     print "after the --args argument."
#     print "Usage: bokeh serve graphing --show --args <path to the regression data> <graph in 3D [True | False]>"
#     UpsamplerGraph()
